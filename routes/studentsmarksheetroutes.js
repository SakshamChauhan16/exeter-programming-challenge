const {getItems, addItem, deleteItem, updateItem} = require('../controllers/studentsmarksheetcontroller')


const Item ={
    type:'object',
    properties:{
        StudentName: {type: 'string'},
        StudentID: {type: 'string'},
        Subject1: { type: 'string'},
        Subject2: {type: 'string'},
        Subject3: {type: 'string'},
        Subject4: {type: 'string'},
        Subject5: {type: 'string'}
    }
}
//options for get all items
const getItemsOpts = {
    schema:{
        response:{
            200:{
                type: 'array',
                items: Item
            },
        },
    },
    handler: getItems
}

//options to add items
const postItemsOpts ={
    schema: {
        body: {
            type:'object',
            required: ['StudentName','Subject1','Subject2','Subject3',
            'Subject4','Subject5'],
            properties:{
                StudentName: {type:'string'},
                Subject1: { type: 'string'},
                Subject2: {type: 'string'},
                Subject3: {type: 'string'},
                Subject4: {type: 'string'},
                Subject5: {type: 'string'}
            }


        },
        response:{
            201: Item,
        },
    },
    handler: addItem
}

const deleteItemsOpts = {
    schema: {
        response:{
            200: {
                type: 'object',
                properties: {
                    message: {type: 'string'}
                },
            },
        },

    },
    handler: deleteItem
}

const updateItemOpts = {
    schema: {
        response:{
            200:Item,
        },
    },
    handler: updateItem
}

function itemRoutes(fastify,options, done){
    //Add items
    fastify.post('/studentmarksheet', postItemsOpts)

    //Update items
    fastify.put('/studentmarksheet/:StudentID/:StudentName', updateItemOpts)
    
    //Delete items
    fastify.delete('/studentmarksheet/:StudentID', deleteItemsOpts)

    //Get all items
    fastify.get('/studentmarksheet',getItemsOpts)

    done()
}

module.exports = itemRoutes