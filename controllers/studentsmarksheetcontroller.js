let students = require('../StudentMarksheetData')

const getItems = (req,reply) =>{
    reply.send(students)
}

const addItem = (req,reply) => {
    const {StudentName} = req.body
    const {StudentID} = req.body
    const {Subject1} = req.body
    const {Subject2} = req.body
    const {Subject3} = req.body
    const {Subject4} = req.body
    const {Subject5} = req.body
    const student = {
        StudentName,
        StudentID,
        Subject1,
        Subject2,
        Subject3,
        Subject4,
        Subject5


    }
    students=[...students, student]

    reply.code(201).send(student)
}

const deleteItem = (req, reply) => {
    const {StudentID} = req.params

    students = students.filter((student) => student.StudentID !== StudentID)

    reply.send({message: `The whole data corresponding to StudentID- ${StudentID} has been removed`})
}

const updateItem = (req, reply) => {
    const {StudentName} = req.params
    const {StudentID} = req.params
    const {Subject1} = req.body
    const {Subject2} = req.body
    const {Subject3} = req.body
    const {Subject4} = req.body
    const {Subject5} = req.body

    students = students.map(student => (student.StudentID === StudentID ? {StudentName,StudentID,Subject1,Subject2,Subject3,Subject4,Subject5}
        :student))

    student = students.find((student) => student.StudentID === StudentID)

    reply.send(student)
}

module.exports = {getItems,addItem,deleteItem,updateItem}